(function( $ ) {
    $(window).resize(function(){
        anchoMenu=$(".card-obverse").width();
        altoMenu=anchoMenu * 0.55;
        $(".card-obverse").css("height", altoMenu);
        $(".card-reverse").css("height", altoMenu);
    });

    // Envio de formulario mediante el boton
    $(document).on('click','.entry-button',function(){
        $.validator.addMethod("validateMonth", function (value) {
            if(value > 0 && value < 13) {
                return true;
            }
            return false;
        }),
        $(".entry-form").validate({
            rules: {
                name: { required: true, minlength: 1},
                number: { required:true, minlength: 16, maxlength: 16},
                month: { required:true, validateMonth: true},
                year: { required:true, minlength: 2, maxlength: 2},
                cvc: { required:true, maxlength: 3, minlength: 3}
            },
            messages: {
                name: "Can't be blank",
                number : "Wrong format, numbers only",
                month : "Can't be blank",
                year : "Can't be blank",
                cvc : "Can't be blank",
            },
            errorElement : 'span',
            submitHandler: function () {
                $(".entry-form").css("display", "none");
                $(".entry-success").css("display", "grid");
                // Añadir valores introducidos a las tarjetas
                $form_values = getFormData();
                setValues($form_values);
            },
            invalidHandler: function () {
                $form_values = getFormData();
                setValues($form_values);
            }
        });
    });
    function getFormData(){
        var config = {};
        $('input').each(function () {
            config[this.name] = this.value;
        });
        return(config);
    }
    function setValues(form_values){
        $name = form_values.name;
        if($name!='') $(".card-name").text($name);
        $number = [...form_values.number].reduce((p, c, i) => p += (i && !(i % 4)) ? " " + c : c, "");
        if($number!='' && form_values.number.length == 16) $(".card-number").text($number);
        $month = form_values.month;
        $year =form_values.year;
        $date = $month+'/'+$year;
        if($year!='' && $month!='' && $year.length == 2 && $month.length == 2 && $month < 13 && $month > 0 ) $(".card-date").text($date);
        $cvc = form_values.cvc;
        if($cvc!='') $(".card-cvc").text($cvc);
    }
})( jQuery );